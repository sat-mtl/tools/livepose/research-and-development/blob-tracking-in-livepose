from typing import Any, List

from livepose.dataflow import Channel, Flow, Stream
from livepose.filter import register_filter, Filter
from livepose.pose_backend import PosesForCamera


@register_filter("blobs")
class BlobsFilter(Filter):
    """
    Filter which outputs the full skeleton for each person detected by camera, based on
    the BODY_25 dataset.
    For more information about the bones, see:
    https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering

    OSC message is as follows, for each person and body part:
    {base OSC path}/skeleton/{camera_id}/{Person ID}/{Body Part} {screen X position} {screen Y position} {confidence value}
    """

    def __init__(self, *args: Any, **kwargs: Any):
        super(BlobsFilter, self).__init__("blobs", **kwargs)

    def add_parameters(self) -> None:
        super().add_parameters()
        self._parser.add_argument("--min-pose-completeness", type=float, default=0.0, help="Minimum pose completeness")
        self._parser.add_argument("--two-dimensional", type=bool, default=True, help="Two dimensional")
        self._parser.add_argument("--three-dimensional", type=bool, default=False, help="Three dimensional")

    def init(self) -> None:

        self._show_2d_poses = self._args.two_dimensional
        self._show_3d_poses = self._args.three_dimensional
        self._min_pose_completeness = self._args.min_pose_completeness

    def step(self, flow: Flow, now: float, dt: float) -> None:
        """
        Update the filter
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        super().step(flow=flow, now=now, dt=dt)

        result: Filter.Result = {}

        # Get all pose streams
        pose_streams = flow.get_streams_by_type(Stream.Type.POSE_BACKEND)
        pose_streams.update(flow.get_streams_by_type(Stream.Type.DIMMAP))

        # All poses from all streams are sent
        for stream in pose_streams.values():
            assert(stream is not None)

            channels: List[Channel] = []
            if self._show_2d_poses:
                channels += stream.get_channels_by_type(Channel.Type.POSE_2D)
            if self._show_3d_poses:
                channels += stream.get_channels_by_type(Channel.Type.POSE_3D)

            for channel in channels:
                blobs_for_cameras: List[BlobsForCamera] = channel.data

                for cam_id, blobs_for_camera in enumerate(blobs_for_cameras):
                    result[cam_id] = {}
                    for blob_index, blob in blobs_for_camera.blobs.items():
                        # Check whether enough keypoints are detected for this pose

                        result[cam_id][blob_index] = {}
                        result[cam_id][blob_index]["start_pose"] = [now, dt]

                        blob_id = blob.id
                        if blob_id is not None:
                            result[cam_id][blob_index]["blob_id"] = blob_id

                        # Iter over each keypoint.
                        result[cam_id][blob_index]["center1"] = [blob.center[0]]
                        result[cam_id][blob_index]["center2"] = [blob.center[1]]
                        result[cam_id][blob_index]["size"] = blob.size

                        result[cam_id][blob_index]["end_pose"] = [now, dt]

        self._result = result

        if not flow.has_stream(self._filter_name):
            flow.add_stream(name=self._filter_name, type=Stream.Type.FILTER)

        flow.set_stream_frame(
            name=self._filter_name,
            frame=[Channel(
                type=Channel.Type.OUTPUT,
                data=self._result,
                name=self._filter_name,
                metadata={}
            )]
        )
