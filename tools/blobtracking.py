import cv2

# Create a SimpleBlobDetector parameters object
params = cv2.SimpleBlobDetector_Params()

# Modify the parameters as needed
params.minThreshold = 10
params.maxThreshold = 50
params.filterByArea = True
params.minArea = 60
params.filterByColor = True
params.blobColor = 255
params.minDistBetweenBlobs = 80
params.filterByCircularity = False
params.filterByInertia = False
params.filterByConvexity = False


# Create a SimpleBlobDetector with the parameters
detector = cv2.SimpleBlobDetector_create(params)

# set min size of tracked object, e.g. 15x15px
parameter_lucas_kanade = dict(winSize=(15, 15), maxLevel=4, criteria=(cv2.TERM_CRITERIA_EPS |
                                                                      cv2.TERM_CRITERIA_COUNT, 10, 0.03))


# Open a video capture object
cap = cv2.VideoCapture('/home/metalab_legion/Desktop/tests2209/randomaction.avi')  # Replace 'your_video_file.avi' with your video file path

if not cap.isOpened():
    print("Error: Could not open video file")
    exit()

background = cv2.imread('/home/metalab_legion/Desktop/tests2209/out.png')

#initialize old frame
frame_init = background

#initialize keypoints
keypoints = ([[]])

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    
    # substract background
    mask = cv2.subtract(frame, background)
    # Convert the frame to grayscale
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #ret, thresh = cv2.threshold(gray, 15, 50, cv2.THRESH_BINARY)
    # Detect blobs in the grayscale frame

    #contours1, hierarchy1 = cv2.findContours(image=thresh, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)
    #print(contours1)
    # draw contours on the original image
    #cv2.drawContours(image=frame, contours=contours1, contourIdx=-1, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_AA)
    old_points = keypoints

    print(keypoints.pt)
    if len(old_points) > 2:
        new_points, status, errors = cv2.calcOpticalFlowPyrLK(frame_init, frame, keypoints, None,
                                                            **parameter_lucas_kanade)
        for index, keypoint in enumerate(keypoints):
            cv2.line(frame, new_points[index], keypoint, (0, 255, 0), 3)
        
        frame_init = frame.copy()
    keypoints = detector.detect(mask)

    # Draw the keypoints on the frame
    frame_with_keypoints = cv2.drawKeypoints(frame, keypoints, outImage=None, color=(0, 0, 255),
                                             flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)



    # Display the frame with keypoints
    #cv2.imshow('Frame with Keypoints', frame_with_keypoints)
    cv2.imshow('frame', frame_with_keypoints)

    #cv2.imshow('frame with background removed', mask)
    # Press 'q' to exit the loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the video capture object and close the OpenCV windows
cap.release()
cv2.destroyAllWindows()
