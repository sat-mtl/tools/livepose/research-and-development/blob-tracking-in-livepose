# How to use this repository

This repository contains code to run a blobtracking system in realtime, based on a camera input. 

### TODO : PUT A VIDEO HERE OF THE SYSTEM RUNNING REALTIME

# nearest_neighbour.py

To run the script, simply edit the dev_id argument (either video or webcam) and waittime argument in detection_context(). By default, this will send id blob position to OSC port 9000. 

For realtime usage, waittime should be set to 0. For video replay usage, something along the lines of 25 should work well. 

You might need to install the python modules specified in the script

# Real time usage

This repository has originally been created for Marie Leblanc Flanagan's residency with the SAT research centre. In this residency, the artist proposed different game designs to be played by individuals in a dome. 

In order to do so, we placed a camera at the Zenith of the dome, a 