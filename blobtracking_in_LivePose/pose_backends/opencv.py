import logging
import os
import sys

from operator import itemgetter
from typing import Any, Dict, List, Optional, Tuple

from dataclasses import dataclass

from livepose.extlib.sort import Sort

import cv2
import numpy as np

from livepose.dataflow import Channel, Flow, Stream
from livepose.pose_backend import PoseBackend, register_pose_backend

logger = logging.getLogger(__name__)

@dataclass
class Blobs:
    def __init__(self, id: int, center: List[float], size: float) -> None:
        self.id = id 
        self.center = center
        self.size = size
    
    id: int
    center: List[float]
    size: float

@dataclass
class BlobsForCamera:
    def __init__(self, blobs: List[Blobs]) -> None:
        self.blobs = blobs

    blobs: List[Blobs]

class NearestBlob: 
    def __init__(self):

        self._prev_blobs : List = []
        self._num_frame: int = 0

    def nearest(self, blobs_by_cam: BlobsForCamera):

        source_blob: List = []
        for i in range(len(self._prev_blobs)):
                source_blob.append(-1) 
        dest_blob: np.array = source_blob

        if self._num_frame < 10:
            #TODO replace by a cumulative integration of blobs rather than destroying it every frame

            prev_blobs: List = []
            for blob in blobs_by_cam.blobs: 
                prev_blobs.append([blob.center[0], blob.center[1]])
            self._num_frame += 1
            self._prev_blobs = prev_blobs
            return self._prev_blobs

        min_dist_prev: np.array = np.ones(len(self._prev_blobs)) * -1.0
        
        temp_new_blobs: np.array = self._prev_blobs

        for blob in blobs_by_cam.blobs:
            remove_blob: int = -1
            center = blob.center
            index: int = -1
            nearest_index: int = -1
            min_dist_blob: float = -1.0

            for i in range(len(self._prev_blobs)):
                index += 1
                new_dist = pow(center[0] - self._prev_blobs[i][0], 2) + pow(center[1] - self._prev_blobs[i][1],2)

                if new_dist < min_dist_blob or min_dist_blob < 0:
                    min_dist_blob = new_dist
                    nearest_index = index

            if nearest_index > -1:
                if min_dist_blob < min_dist_prev[nearest_index] or min_dist_prev[nearest_index] < 0:
                    min_dist_prev[nearest_index] = min_dist_blob

                    #update blob position
                    temp_new_blobs[nearest_index] = [center[0], center[1]]

            else:
                if len(self._prev_blobs) < len(blobs_by_cam.blobs):
                    self._prev_blobs.append([center[0], center[1]])
            #         if source_blob[nearest_index] > -1:
            #             remove_blob = source_blob[nearest_index]
            #         source_blob[nearest_index] = blob.id

            # dest_blob[blob.id] = nearest_index
            # if remove_blob > -1:
            #     dest_blob[remove_blob] = -1
        self._prev_blobs = temp_new_blobs

        # if len(self._prev_blobs) < len(blobs_by_cam.blobs):
        #     for i in range(len(source_blob)):
        #         if source_blob[i] == -1:
        #             add_center = blobs_by_cam.blobs[i].center
        #             self._prev_blobs.append([add_center[0], add_center[1]])

        
        #print(min_dist_prev)
        print(self._prev_blobs)

        return self._prev_blobs

@register_pose_backend("opencv")
class OpenCVBackend(PoseBackend):
    """
    opencv backend, for integration of standard computer vision detection techniques such as blob tracking.
    """

    @dataclass
    class CV2Keypoints:
        image: np.array
        keypoints: Any


    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

    def add_parameters(self) -> None:
        # opencv processes no blobs so no parameters are needed even from base class
        super().add_parameters()
        
        # option to add an image for background subtraction
        #TO DO: replace default by NONE
        self._parser.add_argument('--background-file', type=str, default='/home/metalab_legion/Desktop/tests2209/out.png',
                                  help="option to add an image for background subtraction" )

    def init(self) -> None:

        # Create a SimpleBlobDetector parameters object 
        self._params = cv2.SimpleBlobDetector_Params()

        # TO DO: assign params from config file
        self._params.minThreshold = 10
        self._params.maxThreshold = 50
        self._params.filterByArea = True
        self._params.minArea = 60
        self._params.filterByColor = True
        self._params.blobColor = 255
        self._params.minDistBetweenBlobs = 80
        self._params.filterByCircularity = False
        self._params.filterByInertia = False
        self._params.filterByConvexity = False

        # Create a SimpleBlobDetector with the specified parameters
        self._detector = cv2.SimpleBlobDetector_create(self._params)
        
        # initiate background image if specified in the config
        if self._args.background_file:
            self._has_background = True
            self._background = cv2.imread(self._args.background_file)
        else:
            self._has_background = False

        #attribute to hold cv2 keypoints detection result
        self._cv2_keypoints: List[OpenCVBackend.CV2Keypoints] = []
        self._nearestBlob = NearestBlob()

    def draw_objects(self) -> None:
        """
        Draw detected objects onto the source image using cv2.drawKeypoints
        """
        tracked_images = []

        for index, detection_result in enumerate(self._cv2_keypoints):
            tracked_image = cv2.drawKeypoints(
                detection_result.image,
                detection_result.keypoints,
                outImage = None,
                color = (0, 0, 255),
                flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
            )
            # for debugging id_tracking
            blobs_by_cam = self._blobs_for_cameras[index]
            for blob in blobs_by_cam.blobs:
                cv2.putText(
                    tracked_image,
                    str(blob.id),
                    (int(blob.center[0]), int(blob.center[1])),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (255,255,255),
                    5,
                    cv2.LINE_AA
                )
            tracked_images.append(cv2.resize(tracked_image, (1080,1080)))

        self._tracked_images = tracked_images

    def start(self) -> bool:
        """Start the opencv deep learning model.
        :return: bool - success of start. Returns False if there is no valid
        image generator, model path, etc.
        """
        return True

    def step_pose_detection(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model
        :param flow: Flow - Data flow to read from and write to
        :param now: float - current time
        :param dt: float - time since last call
        :return: bool - success
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)

        cv2_keypoints: List[OpenCVBackend.CV2Keypoints] = []

        if len(input_streams) == 0:
            return False

        blobs_by_cam: List[BlobsForCamera] = []

        for stream in input_streams.values():
            if Channel.Type.COLOR not in stream.channel_types:
                return False

            #only valid for one background for now, could make it more specific in the future
            image: np.array = stream.get_channels_by_type(Channel.Type.COLOR)[0].data

            if self._has_background:
                mask = cv2.subtract(image, self._background)
                detected_keypoints = self._detector.detect(mask) 
            else:
                detected_keypoints = self._detector.detect(image)

            
            # Fill blob data per camera
            blobs: List[Blobs] = []
            for id, keypoint in enumerate(detected_keypoints):

                blob: Blobs = Blobs(
                    id = id,
                    center = keypoint.pt,
                    size = keypoint.size
                )
                blobs.append(blob)
            blobs_by_cam.append(BlobsForCamera(
                blobs = blobs
            ))

            #keep the results for drawing on the image
            cv2_keypoints.append(OpenCVBackend.CV2Keypoints(
                image = mask,
                keypoints = detected_keypoints
            ))

        self._blobs_for_cameras = blobs_by_cam
        
        self._cv2_keypoints = cv2_keypoints  

        return True

    def get_blobs(self) -> List[np.array]:

        pass

    # def get_bounding_boxes(self) -> List[np.array]:
    #     """
    #     Get coordinates for bounding boxes containing each pose detected in
    #     each camera image.
    #     """
    #     bounding_boxes = []
    #     for blobs_for_camera in self._blobs_for_cameras:
    #         bounding_boxes_for_cam = []
    #         for blob in blobs_for_camera.blobs:
    #             center = blob.center
    #             size = blob.size


    #             confidence = 0.5 #arbitrary number for now?

    #             min_x = center[0] + np.cos(5/4*np.pi)*size
    #             min_y = center[1] + np.sin(5/4*np.pi)*size
    #             max_x = center[0] + np.cos(1/4*np.pi)*size
    #             max_y = center[1] + np.sin(1/4*np.pi)*size

    #             bounding_boxes_for_cam.append(np.array([
    #                 min_x, min_y, max_x, max_y, confidence
    #             ]))

    #         if bounding_boxes_for_cam:
    #             bounding_boxes.append(np.array(bounding_boxes_for_cam))
    #         else:
    #             # NOTE: The SORT Object Tracker requires an array with shape
    #             # (n, 5) for every frame, even if there are no detections.
    #             bounding_boxes.append(np.empty((0, 5)))

    #     return bounding_boxes

    def step(self, flow: Flow, now: float, dt: float) -> bool:
        """
        Process the given image(s) with the DL Backend model.
        This method should not be overloaded by the pose backends, which
        should instead define their own step_pose_detection method.
        :param flow: Flow - Data flow to read from and write to
        :param now: Current time
        :param dt: Time since last call
        """
        input_streams = flow.get_streams_by_type(Stream.Type.INPUT)
        num_streams = len([True for stream in input_streams.values(
        ) if Channel.Type.COLOR in stream.channel_types])
        #self.initialize_object_trackers(num_streams)

        res = self.step_pose_detection(flow, now, dt)
        if not res:
            return False

        # Compute the bounding boxes, and do the tracking
        #bounding_boxes = self.get_bounding_boxes()
        #self._bounding_boxes = self.update_object_trackers(bounding_boxes)

        # Adjust the pose ids
        # for cam_index, blobs_by_cam in enumerate(self._blobs_for_cameras):
        #     associations = []
        #     source, dest = self._nearestBlob.nearest( blobs_by_cam=blobs_by_cam)

        #     for index in range(len(dest)):
                
        #         associations.append([index, dest[index]])

        #     if self._args.compact_track_ids:
        #         associations = sorted(associations, key=itemgetter(1))
        #         for index, association in enumerate(associations):
        #             association[1] = index

        #     for source_id, dest_id in associations:
        #         if source_id > len(self._blobs_for_cameras):
        #             continue

        #         self._blobs_for_cameras[cam_index].blobs[source_id].id = dest_id

        for cam_index, blobs_by_cam in enumerate(self._blobs_for_cameras):
            neighbour_blobs = self._nearestBlob.nearest( blobs_by_cam=blobs_by_cam)

            if len(neighbour_blobs[0]) > 1:
                for index, blob in enumerate(blobs_by_cam.blobs):

                    if index < len(neighbour_blobs):
                        self._blobs_for_cameras[cam_index].blobs[index].center = [neighbour_blobs[index][0], neighbour_blobs[index][1]]

        return True 

    def get_output(self) -> List[Channel]:
        channels: List[Channel] = [
            Channel(
                type=Channel.Type.POSE_2D,
                name= "blobs",
                data= self._blobs_for_cameras,
                metadata={}
            )
        ]
        if self._tracked_images:
            for index, tracked_image in enumerate(self._tracked_images):
                channels.append(Channel(
                    type=Channel.Type.COLOR,
                    name=f"pose_image_{index}",
                    data=tracked_image,
                    metadata={
                        "resolution": [tracked_image.shape[1], tracked_image.shape[0]]
                    }
                ))

        

        return channels

    def is_gpu_acceleration_active(self) -> bool:
        """Check if GPU acceleration is active."""
        return False

    def is_gpu_acceleration_needed(self) -> bool:
        """Check if GPU acceleration is needed."""
        return False
