## Tracking exploration 

In this subdirectory, we'll find example scripts + resources related to our explorations with tracking.

### Motivation and high level objectives

In the context of an immersive venue, we'll define ID tracking as the capability of extracting a localisation profile of the participants for a given period of time. 

The integration of a tracking system within an immersive experience therefore opens opportunities regarding the customization of an experience for the participants. If participants exist as unique entities within the dataflow system behind the experience, one can imagine that arbitrary and/or evolutive characteristics are assigned to each data profile generated, which may be reflected in different interactive aspects through the experience.

Within a system of connected venues, tracking capabilities may be used in the hope of achieving copresence. We can think, for instance, of a basic set-up where participants are represented as spotlights in an another venue. If the mapping is credible and the tracking system is stable, one observer in the other venue that is unaware of the mechanism might very well agree that there is a life-like behavior to the motion of the lights. 

### Tracking for hybrid venues / technical motivations

We deliberately conceive POCs for tracking in a computer vision framework. A first argument is material: as we're developping a toolbox for hybrid venues, we prioritize technical solutions that require not much more than off the shelf material. A second argument is adaptivity: we wish to develop technical solutions that can be reused in a plurality of contexts. 

Typical performance metrics for tracking systems assessment can be found in [State of the art in tracking](https://www.isical.ac.in/~sankar/paper/DL-MO-2021.pdf) articles. In the deep learning community, performance of tracking system is based on benchmark datasets such as MOT2015 and MOT2016. Detailed explanations of the HOTA metrics can also be [found here](https://jonathonluiten.medium.com/how-to-evaluate-tracking-with-the-hota-metrics-754036d183e1). As we conceive such 

### Scripts / POCs

#### blobtracking.py

#### chatgpt_blob.py

#### optical_flow_sparse_manual.py

#### nearestneighbor.py

