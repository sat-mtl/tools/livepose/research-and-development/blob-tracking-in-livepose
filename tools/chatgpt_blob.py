import cv2
import numpy as np

# Function to detect blobs using SimpleBlobDetector
def detect_blobs(frame):
    # Setup SimpleBlobDetector parameters
    params = cv2.SimpleBlobDetector_Params()

    # Change thresholds if needed
    params.minThreshold = 10
    params.maxThreshold = 50
    params.filterByArea = True
    params.minArea = 60
    params.filterByColor = True
    params.blobColor = 255
    params.minDistBetweenBlobs = 80
    params.filterByCircularity = False
    params.filterByInertia = False
    params.filterByConvexity = False


    # Create detector
    detector = cv2.SimpleBlobDetector_create(params)

    # Detect blobs
    keypoints = detector.detect(frame)

    keypoints = [keypoint.pt for keypoint in keypoints]
    keypoints = np.array(keypoints, dtype = np.float32)
    return keypoints

# Function to track blobs using calcOpticalFlowPyrLK
def track_blobs(prev_frame, current_frame, prev_keypoints):
    # Convert frames to grayscale
    prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
    current_gray = cv2.cvtColor(current_frame, cv2.COLOR_BGR2GRAY)

    # set min size of tracked object, e.g. 15x15px
    parameter_lucas_kanade = dict(winSize=(15, 15), maxLevel=4, criteria=(cv2.TERM_CRITERIA_EPS |
                                                                      cv2.TERM_CRITERIA_COUNT, 10, 0.03))


    # Calculate optical flow using Lucas-Kanade method
    next_keypoints, status, _ = cv2.calcOpticalFlowPyrLK(prev_gray, current_gray, prev_keypoints, None,
                                                    **parameter_lucas_kanade)

 
    # Filter out points where optical flow tracking failed
    valid_keypoints = [keypoint for keypoint, stat in zip(next_keypoints, status) if stat == 1]
    valid_keypoints = np.array(valid_keypoints, dtype = np.float32)
    return valid_keypoints

# Function to find the nearest blob to a given point
def find_nearest_blob(point, blobs):
    distances = [np.linalg.norm(np.array(point.pt) - np.array(blob.pt)) for blob in blobs]
    min_distance_index = np.argmin(distances)
    return blobs[min_distance_index]

# Open video capture
cap = cv2.VideoCapture('/home/metalab_legion/Desktop/tests2209/randomaction.avi')  # Replace 'your_video.mp4' with the path to your video file

# Read the first frame
ret, prev_frame = cap.read()

# Detect blobs in the first frame

prev_keypoints = detect_blobs(prev_frame)

while True:
    # Read the current frame
    ret, current_frame = cap.read()
    if not ret:
        break

    # Track blobs using optical flow
    current_keypoints = track_blobs(prev_frame, current_frame, prev_keypoints)
    # If optical flow tracking fails, assign the nearest blob
    if len(current_keypoints) < len(prev_keypoints):
        print('oupsi')
        # missing_keypoints = set(prev_keypoints) - set(current_keypoints)
        # for missing_keypoint in missing_keypoints:
        #     nearest_blob = find_nearest_blob(missing_keypoint.pt, current_keypoints)
        #     current_keypoints.append(nearest_blob)
    
    # Update the previous frame and keypoints for the next iteration
    prev_frame = current_frame
    prev_keypoints = current_keypoints

    # Draw the keypoints on the current frame
    for i, (current, prev) in enumerate(zip(current_keypoints, prev_keypoints)):
        a, b = current.ravel()
        c, d = prev.ravel()

        # draw line between old and new corner point with random colour
        current_frame = cv2.line(current_frame, (int(a), int(b)), (int(c), int(d)), (0, 255,0), 3)
        # draw circle around new position
        current_frame = cv2.circle(current_frame, (int(a), int(b)), 5, (0,0,255), -1)


    # Display the frame
    cv2.imshow('Blob Tracking', current_frame)
    # Break the loop if 'q' key is pressed
    if cv2.waitKey(30) & 0xFF == ord('q'):
        break

# Release the video capture object and close all windows
cap.release()
cv2.destroyAllWindows()
