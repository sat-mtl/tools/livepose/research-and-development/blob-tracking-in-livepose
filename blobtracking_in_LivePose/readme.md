## Working Structure for BlobTracking in LivePose

This prototype depends in large part on the [cv::SimpleBlobDetector](https://docs.opencv.org/4.x/d0/d7a/classcv_1_1SimpleBlobDetector.html) class. By integrating the aforementioned algorithm in LivePose, we hope to provide a quickly configurable structure for blob detection + tracking in real time for various use cases. 

In particular, this branch is currently being developped in the context of [Marie Leblanc Flanagan's](https://marieflanagan.com/index.html) residency with the Metalab (SAT). She's interested in using blob tracking data from a bird's-eye view in the creation of an immersive experience. 

The following files have been created: 

* tools/blobtracking.py
* livepose/pose_backends/opencv.py
* livepose/configs/test_blob_tracking.json
* livepose/filters/blobs.py

### tools/blobtracking.py

This script is meant to be run as a stand-alone. It contains the essential elements of the blob detection implementation within LivePose. Note that this script detects blobs at every frame, but do not track them. Note also that we subtract the background from our video in order to detect only the blobs associated with moving objects (people in our case).

### livepose/pose_backends/opencv.py

Pretty much a rewrite of the blobtracking.py script according to LivePose's structure. (TO DO) We created specific data structure for extracting blob information at every frame (position of center (in pixel units) + size of the blob), and used the existing [SORT algorithm](https://github.com/abewley/sort) implementation for a first tracking prototype. 

### livepose/configs/test_blobtracking.json

For now, this file is pretty empty. We will add parameters for finetuning the blobdetection directly from the config. The other parameter, 'background_file', can be specified to remove the background for ensuring detection of moving blobs only. 

### livepose/filters/blobs.py

standard filter for outputting blob data. Current outputs are blob id, blob center, and blob size. 